use std::collections::{HashMap, HashSet};
use std::fmt::Debug;
use std::hash::Hash;

use itertools::Itertools;

use super::parsing::sectors::{SectorNumber, SectorType};
use super::parsing::table::FileAccessTable;
use std::iter::FromIterator;

pub struct SuccRelation<T: Hash + Eq + Copy + Debug> {
    pub succ_map: HashMap<T, T>,
}

impl<T: Hash + Eq + Copy + Debug> SuccRelation<T> {
    pub fn reconstruct_entrypoints(&self) -> HashSet<T> {
        let mut predecessor_map = HashMap::<T, Vec<T>>::new();

        for (pred, succ) in self.succ_map.iter() {
            predecessor_map.entry(*succ).or_default().push(*pred);
        }

        let keys = self.succ_map.keys().cloned().collect::<HashSet<T>>();
        let vals = self.succ_map.values().cloned().collect::<HashSet<T>>();

        keys.union(&vals)
            .filter(|a| !predecessor_map.contains_key(a))
            .cloned()
            .collect()
    }
}

/// For sector types which signify a successor, return a pair `(num, successor)`.
fn maybe_successor_pair(
    num: SectorNumber,
    type_: SectorType,
) -> Option<(SectorNumber, SectorNumber)> {
    type_.maybe_succ().map(|succ| (num, succ))
}

pub fn get_starting_sectors(table: &FileAccessTable) -> Vec<SectorNumber> {
    let succ_map = HashMap::<SectorNumber, SectorNumber>::from_iter(
        table
            .sector_role
            .iter()
            .filter_map(|(&n, &t)| maybe_successor_pair(n, t)),
    );

    let starting_sectors = SuccRelation { succ_map }
        .reconstruct_entrypoints()
        .iter()
        .sorted()
        .cloned()
        .collect_vec();

    info!("Located {} starting sectors.", starting_sectors.len());
    debug!("Starting sectors: {}", starting_sectors.iter().join(" "));

    starting_sectors
}

pub struct ChainTraversal {
    /// Only `false` if the chain ends in something corrupt like `FreeSect`
    pub is_complete: bool,
    pub chain: Vec<SectorNumber>,
}

pub fn traverse_chain(table: &FileAccessTable, sector_num: SectorNumber) -> ChainTraversal {
    let mut chain = Vec::<SectorNumber>::new();
    let mut is_complete = true;
    let mut current_sector = sector_num;
    debug!("Starting traversal with {}.", current_sector);
    loop {
        trace!(
            "At sector {} (type {:?})",
            current_sector,
            table.sector_role.get(&current_sector)
        );
        chain.push(current_sector);
        match table.sector_role.get(&current_sector) {
            Some(SectorType::Allocated { next: None }) => {
                break;
            }
            Some(SectorType::Allocated { next: Some(next) }) => {
                current_sector = *next;
            }
            Some(_) => {
                is_complete = false;
                break;
            } // TODO error „ends incompletely“?
            None => {
                break;
            } // TODO error „found unknown sector“?
        }
    }
    debug!("Traversed along {} items", chain.len());
    ChainTraversal { chain, is_complete }
}
