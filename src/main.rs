extern crate colored;
extern crate itertools;
extern crate libc;
#[macro_use]
extern crate log;
extern crate simple_logger;

use std::fs::OpenOptions;
use std::io::{BufReader, Cursor, Read, Seek, Write};
use std::path::{Path, PathBuf};

use clap::{ArgAction, Parser, Subcommand, ValueEnum};
use colored::{ColoredString, Colorize};
use console::Term;
use itertools::Itertools;
use pretty_hex::*;

use chains::{get_starting_sectors, traverse_chain};
use error::RapportError;
use extract::extract_sects;
use hex_rep::HexRep;
use parsing::sectors::{SectorNumber, SectorNumberOffset, SectorType, SectorType::Allocated};
use parsing::table::FileAccessTable;
use parsing::{
    build_fat, parse_fat_sector, parse_file_sector, parse_header, DirectoryEntry, FatSector, Header,
};

use crate::extract::{extract_chain, extract_directory_tree};
use crate::parsing::directory_tree::binary_tree::tree_from_keyed_collection;
use crate::parsing::FAT_SECTOR_SIZE;

pub mod chains;
pub mod error;
pub mod extract;
pub mod hex_rep;
pub mod parsing;

#[cfg(test)]
mod test;

#[derive(Parser, Debug)]
#[clap(about, version, author)]
struct Rapport {
    #[clap(short, action = ArgAction::Count)]
    verbosity: u8,

    #[clap(subcommand)]
    cmd: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    #[clap(about = "Basic summary based on the header")]
    Header {
        #[clap(value_parser)]
        path: PathBuf,
    },

    Extract {
        #[clap(value_parser)]
        path: PathBuf,
        #[clap(short, long, default_value = "./_extract", value_parser)]
        output_folder: PathBuf,
    },

    Sectors {
        #[clap(value_parser)]
        path: PathBuf,
    },

    #[clap(about = "Parse the information of a dir sector (see header for entrypoint)")]
    DirSector {
        #[clap(value_parser)]
        path: PathBuf,
        #[clap(short)]
        sector: u64,
    },

    #[clap(about = "Print all entires of the directory array of this file")]
    PrintDirChain {
        #[clap(value_parser)]
        path: PathBuf,
    },

    DirStreamInfo {
        #[clap(value_parser)]
        path: PathBuf,
    },

    #[clap(about = "list all directory names in a hexdump")]
    DirEntryNameDebug {
        #[clap(value_parser)]
        path: PathBuf,

        #[clap(long, action)]
        as_hex_stream: bool,
    },

    FatSector {
        #[clap(value_parser)]
        path: PathBuf,
        #[clap(short)]
        sector: u64,
    },

    #[clap(name = "fat", about = "Summary of the FAT")]
    DisplayFat {
        #[clap(value_parser)]
        path: PathBuf,

        #[clap(
            value_enum,
            short,
            long,
            default_value = "short",
            help = "Which format to use. One of short|csv|table."
        )]
        format: FatDisplayFormat,
    },

    FatChain {
        #[clap(value_parser)]
        path: PathBuf,

        #[clap(short)]
        starting_sector: u32,
        //#[clap(short = "f", long = "format", default_value = "short")]
        //format: FatDisplayFormat,
    },

    #[clap(name = "chains", about = "List all the chains")]
    FatChainAnalysis {
        #[clap(value_parser)]
        path: PathBuf,

        #[clap(short = 'v', long = "verbose", help = "Force displaying of all blocks")]
        verbose: bool,
    },

    #[clap(about = "Extract a single chain")]
    ExtractChains {
        #[clap(value_parser)]
        path: PathBuf,

        #[clap(default_value = "all")]
        which: String,

        #[clap(name = "out_folder")]
        out_folder: Option<String>,
    },
}

const TRUNC_AFTER: usize = 5;

/// Our main function.
fn main() -> Result<(), RapportError> {
    unsafe {
        libc::signal(libc::SIGPIPE, libc::SIG_DFL);
    }

    let rapport = Rapport::parse();
    simple_logger::init_with_level(match rapport.verbosity {
        0 => log::Level::Warn,
        1 => log::Level::Info,
        2 => log::Level::Debug,
        3 => log::Level::Trace,
        _ => log::Level::Error,
    })
    .unwrap();

    match rapport.cmd {
        Command::Header { path } => {
            let header = parse_header(&mut load_path(path)?)?;
            print_hdr_summary(&header);
        }
        Command::Extract {
            path,
            output_folder,
        } => {
            // TODO make extract expect a reader as well
            let (mut reader, len) = load_path_with_length(path)?;
            let header = parse_header(&mut reader)?;
            extract_sects(&mut reader, len, header, output_folder)?;
        }

        Command::Sectors { path } => {
            Term::stdout().write_line("To be implemented.")?;
            let path_str = path
                .to_str()
                .ok_or_else(|| RapportError(String::from("")))?;
            Term::stdout().write_line(&format!("Got path: {}", path_str))?;
        }

        Command::DirSector { path, sector } => {
            let parsed_sector = parse_file_sector(&mut load_path(path)?, sector)?;
            for dir_entry in parsed_sector.iter() {
                log::debug!("{:#?}", dir_entry);
                print_dir_entry(dir_entry);
            }
        }

        Command::DirStreamInfo { path } => {
            let mut reader = load_path(path)?;
            let tree = extract_directory_tree(&mut reader)?;
            let dir_map = tree.entries;
            println!("Root: {:?}", tree.root);

            // FIXME ugly hard-code:
            const ENTRY_ID: u32 = 59u32;
            let dir_entry_of_interest = dir_map.get(&ENTRY_ID).unwrap();
            // TODO the binary tree is an implementation detail
            //  – this should be `dir_entry_of_interest.iter_children()`, yielding `DirectoryEntry`s
            //  - these can me `map`ped to
            //  - even more, there should be a `DirectoryTree` struct which takes ownership
            //    over the directory pool and whenever we call `iter_children`,
            //    we can map
            tree_from_keyed_collection(
                dir_entry_of_interest,
                &dir_map,
                &|de: &DirectoryEntry| de.siblings().and_then(|s| s.left_id.regular()),
                &|de: &DirectoryEntry| de.siblings().and_then(|s| s.right_id.regular()),
            )
            .expect("Some references in the directory chain could not be followed")
            .into_iter()
            .for_each(|de| {
                println!("{:?}", de);
            });
            for (i, de) in dir_map.iter() {
                let i = format!("{}: ", i).bold();
                print!("{}", i);
                print_dir_entry(de);
            }
        }

        Command::DirEntryNameDebug {
            path,
            as_hex_stream,
        } => {
            // TODO see  https://github.com/activescott/lessmsi/blob/309f163434e0c76e96b7935e211f4dfd83a6ba6c/src/LessMsi.Core/OleStorage/OleStorageFile.cs#L253C1-L284
            //  to understand the name decoding
            //  even better: https://doxygen.reactos.org/da/db8/dll_2win32_2msi_2table_8c_source.html
            let mut reader = load_path(path)?;
            let tree = extract_directory_tree(&mut reader)?;
            let entry_names = tree
                .entries
                .iter()
                .filter_map(|(i, de)| {
                    use DirectoryEntry::*;
                    match de {
                        Storage { raw_name, .. } => Some((i, raw_name)),
                        Stream { raw_name, .. } => Some((i, raw_name)),
                        _ => None,
                    }
                })
                .sorted_by_key(|(&i, _)| i);
            if !as_hex_stream {
                for (i, name) in entry_names {
                    println!(
                        "item {}: {}",
                        i,
                        name.iter()
                            .flat_map(|n| n.to_le_bytes())
                            .map(|c| format!("{:02x}", c))
                            .join(" ")
                    )
                }
            } else {
                let byte_buf: Vec<_> = entry_names.flat_map(|(_, name)|
                    // TODO prepend `i`
                    name.iter()
                        .flat_map(|n| n.to_le_bytes())
                        .chain(std::iter::once(0u8))
                ).collect();
                std::io::stdout().write_all(&byte_buf)?;
                std::io::stdout().flush()?;
            }
        }

        Command::FatSector { path, sector } => {
            let parsed_sector =
                parse_fat_sector(&mut load_path(path)?, SectorNumber::new(sector as u32))?;
            print_fat_sector(&parsed_sector);
        }

        Command::DisplayFat { path, format } => {
            let mut reader = load_path(path)?;

            let header = parse_header(&mut reader)?;
            let table = build_fat(&mut reader, &header)?;
            match format {
                FatDisplayFormat::Table => print_table(&table),
                FatDisplayFormat::Short => print_fat_short(&table),
                _ => {}
            }
        }

        Command::FatChain {
            path,
            starting_sector,
        } => {
            let mut reader = load_path(path)?;

            let header = parse_header(&mut reader)?;
            let table = build_fat(&mut reader, &header)?;
            print_fat_chain(&mut reader, &table, SectorNumber::new(starting_sector))?
        }

        Command::FatChainAnalysis { path, verbose } => {
            debug!("Starting!");
            let mut reader = load_path(path)?;

            let header = parse_header(&mut reader)?;
            let table = build_fat(&mut reader, &header)?;

            for traversal in {
                get_starting_sectors(&table)
                    .iter()
                    .map(|&sector_num| traverse_chain(&table, sector_num))
                    .sorted_by_key(|t| t.chain.len())
            } {
                let num_sectors = traversal.chain.len();
                if !verbose && num_sectors > TRUNC_AFTER {
                    let num_skipped = num_sectors - TRUNC_AFTER + 1;

                    // first 9 sectors
                    print!("{}", traversal.chain.iter().take(TRUNC_AFTER - 1).join(" "));
                    print!(
                        " ..({} sector{}).. ",
                        num_skipped,
                        if num_skipped > 1 { "s" } else { "" }
                    );
                    print!("{}", traversal.chain.last().unwrap());
                } else {
                    print!("{}", traversal.chain.iter().join(" "));
                }
                if !traversal.is_complete {
                    print!(" BAD ENDING!!");
                }
                println!();
            }
        }

        Command::ExtractChains {
            path,
            which,
            out_folder,
        } => {
            debug!("starting");
            let folder_name = out_folder.unwrap_or_else(|| "./_chains".to_string());
            let folder_path = Path::new(&folder_name);
            if !folder_path.exists() {
                std::fs::create_dir(folder_path)?;
            }

            let mut reader = load_path(path)?;
            let header = parse_header(&mut reader)?;
            let table = build_fat(&mut reader, &header)?;

            let entrypoints_to_extract = {
                if which == "all" {
                    get_starting_sectors(&table)
                } else {
                    let which_int = which
                        .parse::<u32>()
                        .map_err(|_| RapportError(format!("Invalid number: {}", which)))?;
                    vec![SectorNumber::new(which_int)]
                }
            };
            for &sector in entrypoints_to_extract.iter() {
                let chain = traverse_chain(&table, sector).chain;

                let complete_path = folder_path.join(format!("{}.chain", sector));
                info!(
                    "Writing to: {}",
                    complete_path.to_str().unwrap_or("<invalid path>")
                );
                let mut f = OpenOptions::new()
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(complete_path)?;

                extract_chain(&mut reader, chain, &mut f)?;
            }
        }

        Command::PrintDirChain { path } => {
            let mut reader = load_path(path)?;
            let header = parse_header(&mut reader)?;
            let table = build_fat(&mut reader, &header)?;
            let first_sect = header
                .dir_info
                .first_sectors
                .first()
                .expect("no first directory error found");
            print_fat_chain(&mut reader, &table, *first_sect)?
        }
    }

    Ok(())
}

#[allow(dead_code)]
fn print_dir_sectors<'a>(iter: impl Iterator<Item = &'a Option<DirectoryEntry>>) {
    for (i, sec) in iter.enumerate() {
        print!("{}: ", i);
        use DirectoryEntry::*;
        match sec {
            Some(Storage { name, child_id, .. }) => {
                println!("storage '{}' child {}", name, child_id)
            }
            Some(Stream {
                name, stream_size, ..
            }) => {
                println!("stream of size {} ('{}')", stream_size, name)
            }
            Some(UnknownOrUnallocated) => println!("(unallocated)"),
            None => println!("(…could not be parsed)"),
        }
    }
}

fn load_path<T: AsRef<Path>>(path: T) -> Result<BufReader<std::fs::File>, RapportError> {
    let (reader, _) = load_path_with_length(path)?;
    Ok(reader)
}

fn load_path_with_length<T: AsRef<Path>>(
    path: T,
) -> Result<(BufReader<std::fs::File>, u64), RapportError> {
    if !path.as_ref().exists() {
        return Err(RapportError(String::from("Path does not exist!")));
    }
    Ok(parsing::load_path(path)?)
}

fn print_hdr_summary(header: &Header) {
    let expected_sig = "0xd0 0xcf 0x11 0xe0 0xa1 0xb1 0x1a 0xe1";
    let actual_sig = header.signature.to_hex_rep();
    if expected_sig == actual_sig {
        println!("{} {}", "Correct signature:".green(), actual_sig);
    } else {
        println!(
            "{} {} (expected {})",
            "Bad signature:".bold().red(),
            actual_sig,
            expected_sig
        );
    }
    println!(
        "CFBF version {}.{}",
        header.version.major, header.version.minor
    );
    println!("{} {}", "miniFAT info:".bold(), header.mini_fat_info);
    println!("{} {}", "FAT info:".bold(), header.fat_info);
    println!("{} {}", "directory info:".bold(), header.dir_info);
    println!("{} {}", "difat info:".bold(), header.difat_info);
}

fn print_dir_entry(entry: &DirectoryEntry) {
    if let DirectoryEntry::UnknownOrUnallocated = entry {
        println!("(Unallocated)");
        return;
    };
    match entry {
        DirectoryEntry::Storage {
            is_root,
            name,
            raw_name,
            color,
            siblings,
            child_id,
            cls_id,
            state_bits,
        } => {
            let desc = if *is_root { "RootStorage" } else { "Storage" };
            println!("{}: {:?}", desc.bold(), name);
            println!("   (hex: {:?})", raw_name);
            println!("  CLSID {}", cls_id);
            println!(
                "  <{} ← * → {}> ↘ {} [{:?}]",
                siblings.left_id, siblings.right_id, child_id, color
            );
            println!("  State bits: {:?}", state_bits);
        }
        DirectoryEntry::Stream {
            name,
            raw_name,
            color,
            siblings,
            starting_sector_loc,
            stream_size,
        } => {
            println!("{}: {:?}", "Stream".bold(), name);
            println!("   (hex: {:?})", raw_name);
            println!(
                "  <{} ← * → {}> [{:?}]",
                siblings.left_id, siblings.right_id, color
            );
            println!(
                "  size {}, starting at sector {}",
                stream_size,
                starting_sector_loc
                    .map(|sec| format!("{}", sec))
                    .unwrap_or_else(|| "None".to_string())
            );
        }
        _ => unreachable!(),
    };
}

fn print_fat_sector(parsed_sector: &FatSector) {
    println!("i: sector[i+offs]");
    for (i, s) in parsed_sector.sectors.iter().enumerate() {
        println!("{}: {}", i, s);
    }
    //println!("{}", parsed_sector.sectors.iter().map(|s| format!("{}", s)).join(", "));
}

#[derive(Copy, Clone, Debug, ValueEnum)]
enum FatDisplayFormat {
    Table,
    Csv,
    Short,
}

fn print_table(table: &FileAccessTable) {
    println!("{:10}| {:10}", "id", "role");
    println!("{:-<22}", "");
    for (sector, sector_type) in table.sorted_iter() {
        let value_desc = match sector_type {
            Allocated { next: Some(num) } => format!("{:4}({:+4})", num, *num - sector),
            x => format!("{:10}", x),
        };
        println!("{:10}| {:10}", sector, value_desc);
    }

    if table.unknown_fat_sectors().next().is_some() {
        println!(
            "{} {}",
            "Unknown sectors:".bold(),
            table
                .unknown_fat_sectors()
                .map(|s| format!("{}", s))
                .join(", ")
        )
    }
}

fn sector_type_to_char(sector: SectorNumber, sector_type: SectorType) -> ColoredString {
    match sector_type {
        SectorType::Allocated { next: Some(num) } => match num - sector {
            SectorNumberOffset(1) => ColoredString::from("→"),
            _ => "A".bold(),
        },
        SectorType::Allocated { next: None } => "E".blue().bold(),
        SectorType::FatSect => "F".red().bold(),
        SectorType::FreeSect => ColoredString::from("_"),
        SectorType::DifSect => "D".red().bold(),
    }
}

fn print_fat_short(table: &FileAccessTable) {
    for (sector, sector_type) in table.sorted_iter() {
        print!("{}", sector_type_to_char(sector, *sector_type))
    }
    println!();
}

fn print_fat_chain<R: Read + Seek>(
    reader: &mut R,
    table: &FileAccessTable,
    first_sector: SectorNumber,
) -> Result<(), RapportError> {
    let sectors = traverse_chain(table, first_sector).chain;
    for s in &sectors {
        print!("{} ", s);
    }
    let mut buff = Cursor::new(vec![0; FAT_SECTOR_SIZE * sectors.len()]);
    extract_chain(reader, sectors, &mut buff)?;

    println!("{:?}", buff.get_ref().hex_dump());
    Ok(())
}
