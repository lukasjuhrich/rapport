use packed_struct::prelude::PackingError;
use std::fmt::{Display, Error, Formatter};

use crate::parsing;

#[derive(Debug, Default)]
pub struct RapportError(pub String);

impl Display for RapportError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write!(f, "CustomError: {}", self.0)
    }
}

#[derive(Debug, Default)]
pub struct ParsingError(RapportError);

impl ParsingError {
    pub fn new(s: &str) -> ParsingError {
        ParsingError(RapportError(String::from(s)))
    }
}

impl From<String> for RapportError {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl From<ParsingError> for RapportError {
    fn from(e: ParsingError) -> Self {
        e.0
    }
}

impl From<std::io::Error> for RapportError {
    fn from(e: std::io::Error) -> Self {
        RapportError(e.to_string())
    }
}

// this and the next one are not not really necessary:
// the parsing of the dir sectors should be encapsulated in a method to Result<-, ParsingError>
impl From<parsing::ParsingErrors> for RapportError {
    fn from(_: parsing::ParsingErrors) -> Self {
        RapportError::default()
    }
}

impl From<packed_struct::PackingError> for RapportError {
    fn from(e: PackingError) -> Self {
        RapportError(e.to_string())
    }
}
// //

impl From<std::io::Error> for ParsingError {
    fn from(e: std::io::Error) -> Self {
        ParsingError(RapportError(e.to_string()))
    }
}

impl From<packed_struct::PackingError> for ParsingError {
    fn from(e: PackingError) -> Self {
        ParsingError(RapportError(e.to_string()))
    }
}

impl From<parsing::ParsingErrors> for ParsingError {
    fn from(_: parsing::ParsingErrors) -> Self {
        ParsingError::default()
    }
}
