use std::collections::{HashMap, HashSet};

use itertools::Itertools;

use super::sectors::{SectorNumber, SectorType};
use super::FatSector;
//use super::super::chains::PredecessorMap;

pub struct FileAccessTable {
    pub sector_role: HashMap<SectorNumber, SectorType>,

    pub known_fat_sectors: HashSet<SectorNumber>,
}

impl FileAccessTable {
    pub fn empty() -> Self {
        Self {
            sector_role: HashMap::new(),
            known_fat_sectors: HashSet::new(),
        }
    }

    /// Include the partial table from a fat sector, whose first entry concerns
    /// the sector `sector_offset`.
    pub fn add_sector_information(
        &mut self,
        sect: &FatSector,
        sector_number: SectorNumber,
        sector_offset: SectorNumber,
    ) {
        debug!(
            "Adding sector {} describing {{{}..}}",
            sector_number, sector_offset
        );
        self.known_fat_sectors.insert(sector_number);
        self.sector_role.extend(
            sect.sectors
                .iter()
                .enumerate()
                .map(|(i, s)| (sector_offset + (i as u32), *s)),
        );
        let new_max_sector = self.current_max_sector();
        match new_max_sector {
            Some(s) => debug!(
                "New maximum key: {} ↦ {}",
                s,
                self.sector_role.get(&s).unwrap()
            ),
            None => debug!("No maximum key (list empty)"),
        }
    }

    pub fn current_max_sector(&self) -> Option<SectorNumber> {
        self.sector_role.keys().max().copied()
    }

    pub fn sorted_iter(&self) -> impl Iterator<Item = (SectorNumber, &SectorType)> {
        self.sector_role
            .iter()
            .sorted_by_key(|(a, _)| **a)
            .map(|(&a, b)| (a, b))
    }

    pub fn unknown_fat_sectors(&self) -> impl Iterator<Item = SectorNumber> + '_ {
        self.sector_role
            .iter()
            .filter(move |(k, v)| match v {
                SectorType::FatSect => !self.known_fat_sectors.contains(k),
                _ => false,
            })
            .map(|(&k, _)| k)
    }
}
