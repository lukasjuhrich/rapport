use std::fmt;

use itertools::Itertools;
use packed_struct::prelude::PackedStruct;

use super::sectors::SectorType;
use super::ParsingErrors;

#[derive(PackedStruct, Clone)]
#[packed_struct(bit_numbering = "msb0", endian = "lsb")]
pub struct FatSectorStruct {
    pub sec_ids: [u32; 128],
}

pub struct FatSector {
    pub sectors: Vec<SectorType>,
}

impl fmt::Debug for FatSectorStruct {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.debug_struct("FatSectorStruct").field(
            "sec_ids",
            &format!(
                "[{}]",
                self.sec_ids.iter().map(|i| format!("{}", i)).join(", ")
            ),
        );
        Ok(())
    }
}

impl TryFrom<FatSectorStruct> for FatSector {
    type Error = ParsingErrors;
    fn try_from(intermediate: FatSectorStruct) -> Result<Self, Self::Error> {
        (&intermediate).try_into()
    }
}

impl TryFrom<&FatSectorStruct> for FatSector {
    type Error = ParsingErrors;
    fn try_from(intermediate: &FatSectorStruct) -> Result<Self, Self::Error> {
        Ok(FatSector {
            sectors: intermediate
                .sec_ids
                .iter()
                .map(|id| SectorType::from(*id))
                .collect(),
        })
    }
}
