use std::convert::TryFrom;
use std::fmt;
use std::fmt::Debug;

use packed_struct::prelude::PackedStruct;

use super::guid::Guid;
use super::sectors::SectorNumber;
use crate::parsing::ParsingErrors;

/// Intermediate representation of a directory entry
#[derive(PackedStruct, Clone)]
#[packed_struct(bit_numbering = "msb0", endian = "lsb")]
pub struct DirectoryEntryIR {
    /// 64 bytes, i.e. 32 UTF16-endpoints à 2 bytes.  Null-terminated.
    pub directory_entry_name: [u16; 32],

    pub directory_entry_name_length: u16,

    /// - 0x00: Unknown/Allocated
    /// - 0x01: Storage Object
    /// - 0x02: Stream Object
    /// - 0x05: Root Storage Object
    pub object_type: u8,

    /// 0: red, 1: black
    pub color_flag: u8,

    /// Stream ID of the left sibling
    pub left_sibling_id: u32,

    /// Stream ID of the right sibling
    pub right_sibling_id: u32,

    /// Stream ID of some(!) child
    pub child_id: u32,

    /// a GUID (16 bytes).
    /// - if storage- or root object, represents an object class GUID
    /// - must be 0 for stream object
    pub cls_id: [u8; 16], // 32*4 = 8*4*4 = 8*16

    /// user-defined flags. Should be 0 for stream object.
    pub state_bits: u32,

    /// Don't care
    pub creation_time: u64,

    /// Don't care
    pub modified_time: u64,

    /// if stream object, contains first sector
    /// if root storage object, this has nothing to do with the directory structure,
    /// and is instead used to signify the minifat location (i.e. the first minifat sector)
    pub starting_sector_loc: u32,

    pub stream_size: u64,
}

impl Debug for DirectoryEntryIR {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let dirname = std::char::decode_utf16(
            self.directory_entry_name
                .iter()
                .take_while(|r| **r != 0u16)
                .cloned(),
        )
        .map(|r| r.unwrap_or('¿'))
        .collect::<String>();
        f.debug_struct("DirectoryEntry")
            .field("directory_entry_name", &dirname)
            .field("object_type", &self.object_type)
            .field("color_flag", &self.color_flag)
            .field("left_sibling_id", &StreamId::from(self.left_sibling_id))
            .field("right_sibling_id", &StreamId::from(self.right_sibling_id))
            .field("child_id", &StreamId::from(self.child_id))
            .field("cls_id", &Guid(self.cls_id))
            .field("state_bits", &self.state_bits)
            .field("starting_sector_loc", &self.starting_sector_loc)
            .field("stream_size", &self.stream_size)
            .finish()?;
        Ok(())
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct SiblingInfo {
    pub left_id: StreamId,
    pub right_id: StreamId,
}

#[derive(Clone, PartialEq, Eq, Debug)]
pub enum DirectoryEntry {
    /// A directory
    Storage {
        is_root: bool,

        name: String,

        raw_name: Vec<u16>,

        color: Color,

        siblings: SiblingInfo,

        /// Should be 0 for stream object
        child_id: StreamId,

        /// Should be 0 for stream object
        cls_id: Guid,

        state_bits: u32,
    },
    /// A file
    Stream {
        name: String,

        raw_name: Vec<u16>,

        color: Color,

        siblings: SiblingInfo,

        /// if stream object, contains first sector
        /// → Should rather be SectorNumber…
        starting_sector_loc: Option<SectorNumber>,

        stream_size: u64,
    },
    /// Unknown or unallocated.
    UnknownOrUnallocated,
}

impl DirectoryEntry {
    pub fn is_root(&self) -> bool {
        match self {
            Self::Storage { is_root, .. } => *is_root,
            _ => false,
        }
    }

    pub fn siblings(&self) -> Option<SiblingInfo> {
        match self {
            Self::Storage { siblings, .. } => Some(*siblings),
            Self::Stream { siblings, .. } => Some(*siblings),
            _ => None,
        }
    }
}

impl TryFrom<DirectoryEntryIR> for DirectoryEntry {
    type Error = ParsingErrors;

    fn try_from(e: DirectoryEntryIR) -> Result<Self, Self::Error> {
        (&e).try_into()
    }
}

impl TryFrom<&DirectoryEntryIR> for DirectoryEntry {
    type Error = ParsingErrors;

    fn try_from(e: &DirectoryEntryIR) -> Result<Self, Self::Error> {
        const UNALLOC: u8 = 0x00;
        const STORAGE: u8 = 0x01;
        const ROOT_STORAGE: u8 = 0x05;
        const STREAM: u8 = 0x02;
        match e.object_type {
            UNALLOC => Ok(DirectoryEntry::UnknownOrUnallocated),
            STORAGE | ROOT_STORAGE | STREAM => {
                let raw_name: Vec<_> = e
                    .directory_entry_name
                    .iter()
                    .take_while(|r| **r != 0u16)
                    .cloned()
                    .collect();
                let name = std::char::decode_utf16(raw_name.clone())
                    .map(|r| r.unwrap_or('¿'))
                    .collect::<String>();

                let siblings = SiblingInfo {
                    left_id: e.left_sibling_id.into(),
                    right_id: e.right_sibling_id.into(),
                };
                let color = e.color_flag.try_into()?;
                match e.object_type {
                    STORAGE | ROOT_STORAGE => Ok(DirectoryEntry::Storage {
                        is_root: e.object_type == ROOT_STORAGE,
                        name,
                        raw_name,
                        color,
                        siblings,
                        child_id: e.child_id.into(),
                        cls_id: Guid(e.cls_id),
                        state_bits: e.state_bits,
                    }),
                    STREAM => {
                        let starting_sector_loc = if e.starting_sector_loc == 0xffff_ffff {
                            None
                        } else {
                            Some(SectorNumber::new(e.starting_sector_loc))
                        };
                        Ok(DirectoryEntry::Stream {
                            name,
                            raw_name,
                            color,
                            siblings,
                            starting_sector_loc,
                            stream_size: e.stream_size,
                        })
                    }
                    _ => unreachable!(),
                }
            }

            _ => Err(ParsingErrors::Other),
        }
    }
}

#[derive(Debug)]
pub enum DirEntryObjectType {
    UnknownOrUnallocated,
    StorageObject,
    StreamObject,
    RootStorageObject,
    Invalid(u8),
}

impl From<u8> for DirEntryObjectType {
    fn from(num: u8) -> DirEntryObjectType {
        match num {
            0x00 => DirEntryObjectType::UnknownOrUnallocated,
            0x01 => DirEntryObjectType::StorageObject,
            0x02 => DirEntryObjectType::StreamObject,
            0x05 => DirEntryObjectType::RootStorageObject,
            n => DirEntryObjectType::Invalid(n),
        }
    }
}

impl fmt::Display for DirEntryObjectType {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self)
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Color {
    Red,
    Black,
}

impl TryFrom<u8> for Color {
    type Error = ParsingErrors;

    fn try_from(flag: u8) -> Result<Self, Self::Error> {
        match flag {
            0 => Ok(Color::Red),
            1 => Ok(Color::Black),
            _ => Err(ParsingErrors::Other),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum StreamId {
    Regular(u32),
    MaximumRegular,
    NoStream,
}

impl StreamId {
    pub fn regular(&self) -> Option<u32> {
        if let Self::Regular(i) = self {
            Some(*i)
        } else {
            None
        }
    }
}

impl From<u32> for StreamId {
    fn from(i: u32) -> Self {
        match i {
            0xffff_ffff => StreamId::NoStream,
            0xffff_fffa => StreamId::MaximumRegular,
            n => StreamId::Regular(n),
        }
    }
}

impl fmt::Display for StreamId {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        match self {
            StreamId::Regular(n) => write!(f, "{}", n),
            other => write!(f, "{:?}", other),
        }
    }
}
