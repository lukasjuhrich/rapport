use super::{directory_entry, DirectoryEntry};
use std::collections::HashMap;

#[allow(dead_code)]
pub struct DirectoryTree {
    // we need: a map from u32 to a DirectoryNode
    pub entries: HashMap<u32, directory_entry::DirectoryEntry>,
    pub root: Box<directory_entry::DirectoryEntry>,
}

impl TryFrom<Vec<Option<DirectoryEntry>>> for DirectoryTree {
    type Error = String;

    fn try_from(value: Vec<Option<DirectoryEntry>>) -> Result<Self, Self::Error> {
        let dir_map: HashMap<_, _> = value
            .iter()
            .enumerate()
            .filter_map(|(i, s)| s.as_ref().map(|de| (i as u32, de.clone())))
            .collect();
        let root: Box<_> = value
            .iter()
            .flatten()
            .find(|&de| de.is_root())
            .ok_or("No root found in this directory chain!")?
            .clone()
            .into();
        Ok(Self {
            root,
            entries: dir_map,
        })
    }
}

pub mod binary_tree;

trait EntryWithChildren {
    fn children(&self) {}
}
