use std::fmt;
use std::ops::{Add, Sub};

use super::SectorNumberOffset;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SectorNumber(u32);

impl SectorNumber {
    pub fn new(num: u32) -> SectorNumber {
        SectorNumber(num)
    }

    pub fn calculate_offset(self) -> u64 {
        (self.0 + 1) as u64 * 0x200
    }
}

impl fmt::Display for SectorNumber {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}

impl Sub<SectorNumber> for SectorNumber {
    type Output = SectorNumberOffset;

    fn sub(self, rhs: SectorNumber) -> Self::Output {
        SectorNumberOffset::new(self.0 as i64 - rhs.0 as i64)
    }
}

impl Add<u32> for SectorNumber {
    type Output = SectorNumber;

    fn add(self, rhs: u32) -> Self::Output {
        SectorNumber::new(self.0 + rhs)
    }
}

impl From<u32> for SectorNumber {
    fn from(i: u32) -> SectorNumber {
        SectorNumber(i)
    }
}
