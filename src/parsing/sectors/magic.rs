pub const SECTOR_FREE: u32 = 0xffff_ffff;
pub const SECTOR_END_OF_CHAIN: u32 = 0xffff_fffe;
pub const SECTOR_FAT: u32 = 0xffff_fffd;
pub const SECTOR_DIFAT: u32 = 0xffff_fffc;
