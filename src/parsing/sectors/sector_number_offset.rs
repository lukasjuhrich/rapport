use std::fmt;

#[derive(Clone, Copy, Debug)]
pub struct SectorNumberOffset(pub i64);

impl SectorNumberOffset {
    pub fn new(num: i64) -> Self {
        Self(num)
    }
}

impl fmt::Display for SectorNumberOffset {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.0, f)
    }
}
