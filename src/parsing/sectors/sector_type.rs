use super::magic::*;
use super::SectorNumber;
use std::fmt;

#[derive(Clone, Debug, Copy)]
pub enum SectorType {
    FreeSect,
    FatSect,
    DifSect,
    Allocated { next: Option<SectorNumber> },
}

impl SectorType {
    pub fn maybe_succ(self) -> Option<SectorNumber> {
        if let SectorType::Allocated { next: Some(next) } = self {
            return Some(next);
        }
        None
    }
}

impl From<u32> for SectorType {
    fn from(num: u32) -> Self {
        match num {
            SECTOR_FREE => SectorType::FreeSect,
            SECTOR_END_OF_CHAIN => SectorType::Allocated { next: None },
            SECTOR_FAT => SectorType::FatSect,
            SECTOR_DIFAT => SectorType::DifSect,
            num => SectorType::Allocated {
                next: Some(SectorNumber::new(num)),
            },
        }
    }
}

impl fmt::Display for SectorType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SectorType::FreeSect => write!(f, "FreeSect"),
            SectorType::FatSect => write!(f, "FatSect"),
            SectorType::DifSect => write!(f, "DifSect"),
            SectorType::Allocated { next: n } => match n {
                Some(x) => write!(f, "{}", x),
                None => write!(f, "EndOfChain"),
            },
        }
    }
}
