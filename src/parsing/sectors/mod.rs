pub use sector_number::SectorNumber;
pub use sector_number_offset::SectorNumberOffset;
pub use sector_type::SectorType;

pub mod magic;
mod sector_number;
mod sector_number_offset;
mod sector_type;
