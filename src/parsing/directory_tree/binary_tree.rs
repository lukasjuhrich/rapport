use std::collections::HashMap;
use std::fmt;
use std::fmt::Formatter;
use std::hash::Hash;

pub struct BinaryTree<T> {
    #[allow(dead_code)]
    pub root: Box<Node<T>>,
}

impl<T> BinaryTree<T> {
    pub fn iter<'a>(&'a self) -> impl Iterator<Item = &T> + 'a {
        BinTreeIterator::<'a, T>::new(&self.root)
    }
}

impl<T> IntoIterator for BinaryTree<T> {
    type Item = T;
    type IntoIter = BinTreeIntoIterator<T>;

    fn into_iter(self) -> Self::IntoIter {
        BinTreeIntoIterator::new(*self.root)
    }
}

pub fn tree_from_keyed_collection<'tree, T, K, F1, F2>(
    root: &'tree T,
    pool: &'tree HashMap<K, T>,
    left_getter: &F1,
    right_getter: &F2,
) -> Result<BinaryTree<&'tree T>, Error>
where
    F1: Fn(&T) -> Option<K>,
    F2: Fn(&T) -> Option<K>,
    K: Eq + Hash,
{
    Ok(BinaryTree::<&'tree T> {
        root: Box::new(_node_from_keyed_collection(
            root,
            pool,
            left_getter,
            right_getter,
        )?),
    })
}

#[derive(Clone, Debug)]
pub struct Node<T> {
    pub value: T,
    pub left: Option<Box<Node<T>>>,
    pub right: Option<Box<Node<T>>>,
}

impl<T> Node<T> {
    pub fn leaf(value: T) -> Self {
        Node {
            value,
            left: None,
            right: None,
        }
    }

    pub fn new(value: T, left: Option<Box<Node<T>>>, right: Option<Box<Node<T>>>) -> Self {
        Node { value, left, right }
    }
}

pub fn _node_from_keyed_collection<'tree, T, K, F1, F2>(
    root: &'tree T,
    pool: &'tree HashMap<K, T>,
    left_getter: &F1,
    right_getter: &F2,
) -> Result<Node<&'tree T>, Error>
where
    F1: Fn(&T) -> Option<K>,
    F2: Fn(&T) -> Option<K>,
    K: Eq + Hash,
{
    let left_value = left_getter(root)
        .map(|key| pool.get(&key).ok_or(Error {}))
        .transpose()? // takes the error out of the Option<> and leaks it to the outside
        .map(|val| _node_from_keyed_collection(val, pool, left_getter, right_getter))
        .transpose()?
        .map(Box::new);
    let right_value = right_getter(root)
        .map(|key| pool.get(&key).ok_or(Error {}))
        .transpose()? // takes the error out of the Option<> and leaks it to the outside
        .map(|val| _node_from_keyed_collection(val, pool, left_getter, right_getter))
        .transpose()?
        .map(Box::new);
    Ok(Node::new(root, left_value, right_value))
}

#[derive(PartialEq)]
enum ChainItemState {
    Fresh,
    Consumed,
}
pub struct BinTreeIterator<'tree, T> {
    stack: Vec<(ChainItemState, &'tree Node<T>)>,
}

impl<'tree, T> BinTreeIterator<'tree, T> {
    pub fn new(t: &'tree Node<T>) -> Self {
        use ChainItemState::*;
        let mut current = t;
        let mut stack = vec![(Fresh, current)];

        while let Some(x) = &current.left {
            current = x.as_ref();
            stack.push((Fresh, current));
        }
        BinTreeIterator { stack }
    }
}

impl<'tree, T> Iterator for BinTreeIterator<'tree, T> {
    type Item = &'tree T;

    fn next(&mut self) -> Option<Self::Item> {
        use ChainItemState::*;
        match self.stack.last() {
            None => None,
            Some(tuple) => {
                let state = &tuple.0;
                let t = tuple.1;

                if state != &ChainItemState::Fresh {
                    panic!("Last element of stack should always be fresh!");
                }
                let to_yield = &t.value;

                if let Some(x) = &t.right {
                    // set the node we're currently on to `Consumed`
                    self.stack.pop();
                    self.stack.push((Consumed, t));

                    let mut current = x.as_ref();
                    self.stack.push((Fresh, current));
                    while let Some(x) = &current.left {
                        current = x.as_ref();
                        self.stack.push((Fresh, current));
                    }
                } else {
                    self.stack.pop(); // we're the only FRESH thing at the moment ;)
                    while let Some((Consumed, _)) = self.stack.last() {
                        self.stack.pop();
                    }
                }

                Some(to_yield)
            }
        }
    }
}

pub struct BinTreeIntoIterator<T> {
    stack: Vec<Node<T>>,
}

impl<T> BinTreeIntoIterator<T> {
    fn new(root: Node<T>) -> Self {
        Self { stack: vec![root] }
    }
}

impl<T> Iterator for BinTreeIntoIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        let stack = &mut self.stack; // just a shorthand

        // step 1: normalize until last element has no `left`
        while stack.last().is_some_and(|node| node.left.is_some()) {
            let Node { value, left, right } = stack.pop().unwrap();
            let l = left.unwrap();
            //  → inorder: left, value, right
            //  → so add to stack in reverse order: right, value, left
            if let Some(r) = right {
                stack.push(*r)
            }
            stack.push(Node::leaf(value));
            stack.push(*l);
        }

        // step 2: use last element
        stack.pop().map(|node| {
            // normalization guarantees left node to be None
            let Node {
                left: _,
                value,
                right,
            } = node;
            if let Some(r) = right {
                stack.push(*r)
            }
            value
        })
    }
}

#[derive(Debug, Clone)]
pub struct Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "Error when building binary tree")
    }
}

impl std::error::Error for Error {}
