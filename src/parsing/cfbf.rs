use std::fmt;
/// Compound File Binary format structs.
use std::fmt::{Debug, Error, Formatter};

use itertools::Itertools;
use packed_struct::prelude::PackedStruct;

use crate::hex_rep::HexRep;

use super::sectors::magic::{SECTOR_END_OF_CHAIN, SECTOR_FREE};
use super::sectors::SectorNumber;

#[derive(PackedStruct)]
#[packed_struct(bit_numbering = "msb0", endian = "lsb")]
pub struct StructuredStorageHeader {
    /// 0xd0 0xcf 0x11 0xe0 0xa1 0xb1 0x1a 0xe1
    /// Offs. `0x00`
    pub signature: [u8; 8],
    /// reserved, must be zero
    /// Offs. `0x08`
    pub class_id: [u8; 16],

    /// Offs. `0x18`
    pub minor_version: u16,
    /// Also called `major version`, either 3 or 4.  We're expecting 3.
    /// Offs. `0x1a`
    pub dll_version: u16,

    /// Offs. `0x1c`
    pub byte_order: u16,
    /// `ld(size of sectors)` → usually `ld(512) == 9`
    /// Offs. `0x1e`
    pub sector_shift: u16,

    /// `ld(size of minifat sectors)` → MUST be `ld(64) == 6`
    /// Offs. `0x20`
    pub mini_sector_shift: u16,

    /// reserved, must be zero
    /// Offs. `0x22`
    pub reserved: [u8; 6],

    /// number of sects in directory chain  (unused for v3)
    /// Offs. `0x28`
    pub dir_sects_count: u32,
    /// number of sects in FAT chain
    /// Offs. `0x2c`
    pub fat_sects_count: u32,

    /// Offs. `0x30`
    pub dir_sects_start: u32,

    /// must be zero
    /// Offs. `0x34`
    pub transaction_signature: [u8; 4],

    /// max size for a mini stream (usually 4096 bytes)
    /// Offs. `0x38`
    pub mini_sector_cutoff: u32,

    /// offset of first miniFAT sect
    /// Offs. `0x3c`
    pub mfat_start: u32,
    /// number of miniFAT sects
    /// Offs. `0x40`
    pub mfat_sects_count: u32,

    /// offset of first DIFAT sect
    /// Offs. `0x44`
    pub difat_start: u32,
    /// number of DIFAT sects
    /// Offs. `0x48`
    pub difat_sects_count: u32,

    /// The IDs of our FAT sectors.  `SecID 0` always has offset `512/0x200`
    /// Offs. `0x4c`
    pub first_fat_sects: [u32; 109],
}

impl StructuredStorageHeader {
    pub fn mini_sector_info(&self) -> String {
        format!(
            "We have {} miniFAT sectors of 2^{} bits, starting at sector {}.",
            self.mfat_sects_count, self.mini_sector_shift, self.mfat_start
        )
    }

    pub fn first_fat_sects(&self) -> impl Iterator<Item = SectorNumber> + '_ {
        self.first_fat_sects
            .iter()
            .filter(|&s| *s != SECTOR_FREE)
            .map(|s| SectorNumber::new(*s))
    }
}

impl Debug for StructuredStorageHeader {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        f.debug_struct("StructuredStorageHeader")
            .field("dir_sects_count", &self.dir_sects_count)
            .field("fat_sects_count", &self.fat_sects_count)
            .field("difat_start", &self.difat_start.to_hex_rep())
            .field("difat_sects_count", &self.difat_sects_count)
            .field("mfat_sects_count", &self.mfat_sects_count)
            .finish()?;
        Ok(())
    }
}

pub struct VersionInfo {
    pub minor: u16,
    pub major: u16,
}

impl From<&StructuredStorageHeader> for VersionInfo {
    fn from(h: &StructuredStorageHeader) -> Self {
        VersionInfo {
            major: h.dll_version,
            minor: h.minor_version,
        }
    }
}

pub struct SectorInfo {
    pub bit_width: u16,
    pub count: u32,
    pub first_sectors: Vec<SectorNumber>,
}

impl fmt::Display for SectorInfo {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let bit_info = format!("{} sectors of width 2^{}", self.count, self.bit_width);

        if self.first_sectors.is_empty() {
            write!(f, "{}", bit_info)
        } else {
            write!(
                f,
                "{}, starting with {}",
                bit_info,
                self.first_sectors
                    .iter()
                    .map(|s| format!("{}", s))
                    .join(", ")
            )
        }
    }
}

pub struct Header {
    pub signature: [u8; 8],
    pub version: VersionInfo,
    pub mini_fat_info: SectorInfo,
    pub fat_info: SectorInfo,
    pub dir_info: SectorInfo,
    pub difat_info: SectorInfo,
}

fn maybe_single_sector(sector_number: u32) -> Vec<SectorNumber> {
    match sector_number {
        SECTOR_END_OF_CHAIN => vec![],
        num => vec![SectorNumber::new(num)],
    }
}

impl From<&StructuredStorageHeader> for Header {
    fn from(h: &StructuredStorageHeader) -> Self {
        Header {
            signature: h.signature,
            version: VersionInfo::from(h),
            mini_fat_info: SectorInfo {
                bit_width: h.mini_sector_shift,
                count: h.mfat_sects_count,
                first_sectors: maybe_single_sector(h.mfat_start),
            },
            fat_info: SectorInfo {
                bit_width: h.sector_shift,
                count: h.fat_sects_count,
                first_sectors: h.first_fat_sects().collect(),
            },
            dir_info: SectorInfo {
                bit_width: h.sector_shift,
                count: h.dir_sects_count,
                first_sectors: maybe_single_sector(h.dir_sects_start),
            },
            difat_info: SectorInfo {
                bit_width: h.sector_shift,
                count: h.difat_sects_count,
                first_sectors: maybe_single_sector(h.difat_start),
            },
        }
    }
}
