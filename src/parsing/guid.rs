use itertools::Itertools;
use std::fmt;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Guid(pub [u8; 16]);

trait Hexable<T>
where
    T: std::fmt::Display,
{
    fn el_wise_hex(&self) -> String;
}

impl Hexable<u8> for [u8] {
    fn el_wise_hex(&self) -> String {
        self.iter().map(|c| format!("{:02x}", c)).join("")
    }
}

impl fmt::Debug for Guid {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let guid = self.0;
        write!(
            f,
            "{}-{}-{}-{}-{}",
            guid[0..4].el_wise_hex(),
            guid[4..6].el_wise_hex(),
            guid[6..8].el_wise_hex(),
            guid[8..10].el_wise_hex(),
            guid[10..16].el_wise_hex(),
        )
    }
}

impl fmt::Display for Guid {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{:?}", self)
    }
}
