use std::io::{BufReader, Read, Seek, SeekFrom};
use std::path::Path;

use packed_struct::prelude::*;

use crate::error::ParsingError;

pub use self::cfbf::Header;
use self::cfbf::StructuredStorageHeader;
pub use self::directory_entry::{Color, DirEntryObjectType, DirectoryEntry, DirectoryEntryIR};
pub use self::fat_sector::FatSector;
use crate::parsing::fat_sector::FatSectorStruct;
use crate::parsing::table::FileAccessTable;

mod cfbf;
mod directory_entry;
pub mod directory_tree;
mod fat_sector;
pub mod guid;
pub mod sectors;
pub mod table;

pub fn load_path<T: AsRef<Path>>(
    path: T,
) -> Result<(BufReader<std::fs::File>, u64), std::io::Error> {
    let file = std::fs::File::open(path)?;
    let file_len = file.metadata()?.len();
    Ok((BufReader::new(file), file_len))
}

pub fn parse_header<R: Read + Seek>(reader: &mut R) -> Result<Header, ParsingError> {
    reader.seek(SeekFrom::Start(0))?;
    let header: StructuredStorageHeader = {
        let mut buf = [0; 512];
        reader.read_exact(&mut buf)?;
        StructuredStorageHeader::unpack(&buf)?
    };

    Ok(Header::from(&header))
}

pub fn parse_file_sector<R: Read + Seek>(
    reader: &mut R,
    sector: u64,
) -> Result<Vec<DirectoryEntry>, ParsingError> {
    // TODO parse _all_ the entries in a Directory sector!
    // (DirectorySector = Vec<DirectoryEntry> – with sector size 512 (v3), there are 4 entries.
    // Sector 0 is the first one after the header, so adding +1 to get the right offset.
    let mut dir_entries: Vec<DirectoryEntry> = vec![];
    reader.seek(SeekFrom::Start((sector + 1) * 0x200))?;
    for _ in 0..4 {
        let mut buf = [0; 128];
        reader.read_exact(&mut buf)?;
        dir_entries.push(DirectoryEntryIR::unpack(&buf)?.try_into()?);
    }

    Ok(dir_entries)
}

pub enum ParsingErrors {
    Other,
}

pub const FAT_SECTOR_SIZE: usize = 512;

pub fn parse_fat_sector<R: Read + Seek>(
    reader: &mut R,
    sector: sectors::SectorNumber,
) -> Result<FatSector, ParsingError> {
    reader.seek(SeekFrom::Start(sector.calculate_offset()))?;
    let mut buf = [0; FAT_SECTOR_SIZE];
    reader.read_exact(&mut buf)?;
    let fat_sector_struct = FatSectorStruct::unpack(&buf)?;
    Ok(fat_sector_struct.try_into()?)
}

pub fn build_fat<R: Read + Seek>(
    reader: &mut R,
    header: &Header,
) -> Result<FileAccessTable, ParsingError> {
    let mut table = FileAccessTable::empty();
    for s in header.fat_info.first_sectors.iter() {
        let sector = parse_fat_sector(reader, *s)?;
        table.add_sector_information(
            &sector,
            *s,
            table
                .current_max_sector()
                .map(|s| s + 1)
                .unwrap_or_else(|| sectors::SectorNumber::new(0)),
        )
    }

    Ok(table)
}
