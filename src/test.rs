use super::*;
use crate::parsing::directory_tree::binary_tree::{tree_from_keyed_collection, BinaryTree, Node};
use chains::SuccRelation;

macro_rules! map(
    { $($key:expr => $value:expr),* } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
    };
    // extra call to support the trailing comma
    { $($key:expr => $value:expr),+, } => { map! { $($key => $value),+ } };
);

macro_rules! set(
    { $($key:expr),+ } => {
        {
            let mut m = ::std::collections::HashSet::new();
            $(
                m.insert($key);
            )+
            m
        }
    };
);

#[test]
fn test_succ_relation_construction() {
    let my_fat = SuccRelation {
        succ_map: map! {
            1 => 2,
            2 => 3,
            4 => 5
        },
    };
    assert_eq!(my_fat.reconstruct_entrypoints(), set! { 1usize, 4 });
}

macro_rules! sn {
    ( $num:tt ) => {
        SectorNumber::new($num)
    };
}

#[test]
fn test_succ_relation_polymorphism() {
    let my_fat = SuccRelation {
        succ_map: map! {
            sn!(1) => sn!(2),
            sn!(2) => sn!(3),
            sn!(4) => sn!(5)
        },
    };
    assert_eq!(my_fat.reconstruct_entrypoints(), set! { sn!(1), sn!(4) })
}

#[test]
fn test_red_black_tree() {
    let root = ("A", Some(2), Some(3));
    let tree_map = map! {
        1 => root,
        2 => ("B", None, None),
        3 => ("C", None, None),
    };
    let result = tree_from_keyed_collection(&root, &tree_map, &|&(_, l, _)| l, &|&(_, _, r)| r);
    if let Ok(entry) = result {
        let c = entry.iter().map(|&e| e.0).collect_vec();
        assert_eq!(c, vec!["B", "A", "C"]);
    }
}

macro_rules! b {
    ($x:expr) => {
        Some(Box::new($x))
    };
}

macro_rules! t {
    ($val:expr, _, _) => {
        Node {
            value: $val,
            left: None,
            right: None,
        }
    };
    ($val:expr, _, $right: expr) => {
        Node {
            value: $val,
            left: None,
            right: b!($right),
        }
    };
    ($val:expr, $left: expr, _) => {
        Node {
            value: $val,
            left: b!($left),
            right: None,
        }
    };
    ($val:expr , $left: expr, $right: expr) => {
        Node {
            value: $val,
            left: b!($left),
            right: b!($right),
        }
    };
}

macro_rules! l {
    ($val:expr) => {
        t!($val, _, _)
    };
}

#[test]
fn test_iter_0() {
    let root = Box::new(t!(1, l!(2), _));
    assert_eq!(BinaryTree { root }.iter().collect_vec(), vec![&2, &1]);
}

#[test]
fn test_iter_1() {
    let root = Box::new(t!(2, l!(1), l!(3)));
    assert_eq!(BinaryTree { root }.iter().collect_vec(), vec![&1, &2, &3]);
}

#[test]
fn test_iter_2() {
    let root = Box::new(t!(2, t!(1, l!(3), l!(4)), t!(5, l!(6), l!(7))));
    assert_eq!(
        BinaryTree { root }.iter().collect_vec(),
        vec![&3, &1, &4, &2, &6, &5, &7]
    );
}

#[test]
fn test_iter_3() {
    let root = Box::new(t!(4, t!(2, l!(1), l!(3)), t!(6, l!(5), l!(7))));
    assert_eq!(
        BinaryTree { root }.iter().collect_vec(),
        vec![&1, &2, &3, &4, &5, &6, &7]
    );
}

#[test]
fn test_intoiter_1() {
    let root = Box::new(t!(2, l!(1), l!(3)));
    assert_eq!(BinaryTree { root }.into_iter().collect_vec(), vec![1, 2, 3]);
}

#[test]
fn test_intoiter_2() {
    let root = Box::new(t!(2, t!(1, l!(3), l!(4)), t!(5, l!(6), l!(7))));
    assert_eq!(
        BinaryTree { root }.into_iter().collect_vec(),
        vec![3, 1, 4, 2, 6, 5, 7]
    );
}

#[test]
fn test_intoiter_3() {
    let root = Box::new(t!(4, t!(2, l!(1), l!(3)), t!(6, l!(5), l!(7))));
    assert_eq!(
        BinaryTree { root }.into_iter().collect_vec(),
        vec![1, 2, 3, 4, 5, 6, 7]
    );
}
