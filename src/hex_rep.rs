use itertools::Itertools;

pub trait HexRep {
    fn to_hex_rep(&self) -> String;
}

impl HexRep for u8 {
    fn to_hex_rep(&self) -> String {
        format!("0x{:02x}", self)
    }
}

impl HexRep for u32 {
    fn to_hex_rep(&self) -> String {
        format!("0x{:08x}", self)
    }
}

impl HexRep for u64 {
    fn to_hex_rep(&self) -> String {
        format!("0x{:16x}", self)
    }
}

impl<T: HexRep> HexRep for Vec<T> {
    fn to_hex_rep(&self) -> String {
        self.iter().map(|s| s.to_hex_rep()).join(" ")
    }
}

// TODO make macro depending on variable size
impl<T: HexRep> HexRep for [T; 8] {
    fn to_hex_rep(&self) -> String {
        self.iter().map(|s| s.to_hex_rep()).join(" ")
    }
}
