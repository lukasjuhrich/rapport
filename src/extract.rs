use std::fs::{File, OpenOptions};
use std::io::{BufWriter, Cursor, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};
use std::slice;

use dialoguer::Confirm;

use crate::chains::traverse_chain;
use crate::error::RapportError;
use crate::hex_rep::HexRep;
use crate::parsing::directory_tree::DirectoryTree;
use crate::parsing::sectors::SectorNumber;
use crate::parsing::{
    build_fat, parse_header, DirectoryEntry, DirectoryEntryIR, Header, FAT_SECTOR_SIZE,
};
use packed_struct::prelude::*;

pub fn extract_sects<R: Read + Seek>(
    reader: &mut R,
    file_size: u64,
    header: Header,
    output_folder: PathBuf,
) -> Result<(), RapportError> {
    let sector_size = 2u64.pow(header.fat_info.bit_width as u32);
    println!("sector size is {}", sector_size);
    let num_sectors = (file_size - 0x200) / sector_size;

    if num_sectors > 100
        && !Confirm::new()
            .with_prompt(&format!(
                "About to create {} sectors. Continue?",
                num_sectors
            ))
            .interact()?
    {
        return Ok(());
    }

    // let decimal_places = (num_sectors as f16).log(10).floor();

    if !output_folder.exists() {
        std::fs::create_dir("./_extract")?;
    }

    for sect_id in 0..num_sectors - 1 {
        let offset = 0x200 + sect_id * sector_size;
        let filename = format!("sect_{}", sect_id); // TODO use decimal_places
        extract_sect(
            reader,
            offset,
            sector_size,
            Path::new(&format!("./_extract/{}", filename)),
        )?;
    }
    Ok(())
}

pub fn extract_sect<R: Read + Seek>(
    reader: &mut R,
    offset: u64,
    size: u64,
    target_path: &Path,
) -> Result<(), RapportError> {
    println!(
        "Writing {} bytes starting from {}",
        size,
        offset.to_hex_rep()
    );
    if !target_path.exists() {
        File::create(target_path)?;
    }

    reader.seek(SeekFrom::Start(offset))?;
    let buf = reader
        .bytes()
        .take(size as usize)
        .collect::<Result<Vec<_>, _>>()?;
    let slice_u8: &[u8] = unsafe { slice::from_raw_parts(buf.as_ptr(), buf.len()) };

    println!("Writing to {:?}", target_path);

    let mut target_file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(target_path)?;
    target_file.write_all(slice_u8)?;

    Ok(())
}

pub fn extract_chain<R: Read + Seek, W: Write>(
    reader: &mut R,
    chain: Vec<SectorNumber>,
    output_file: &mut W,
) -> Result<(), RapportError> {
    let mut f = BufWriter::new(output_file);
    for &sec in chain.iter() {
        let mut buffer = [0u8; FAT_SECTOR_SIZE];
        reader.seek(SeekFrom::Start(sec.calculate_offset()))?;
        reader.read_exact(&mut buffer)?;

        f.write_all(&buffer)?;
    }

    Ok(())
}

/// Given a reader of the directory stream, return the directory entries.
pub fn extract_dir_entries<R: Read + Seek>(
    reader: &mut R,
) -> Result<Vec<Option<DirectoryEntry>>, RapportError> {
    reader.seek(SeekFrom::Start(0))?;
    let mut dir_entries: Vec<Option<DirectoryEntry>> = vec![];
    const DIR_SECT_SIZE: usize = 128;
    let mut buffer = [0; DIR_SECT_SIZE];
    while let Ok(()) = reader.read_exact(&mut buffer) {
        let entry_ir = DirectoryEntryIR::unpack(&buffer).ok();
        let entry = entry_ir.and_then(|ir| TryInto::<DirectoryEntry>::try_into(ir).ok());
        dir_entries.push(entry);
    }
    Ok(dir_entries)
}

// TODO this has a lot of dependencies. does it belong in this module?
pub fn extract_directory_tree<R: Read + Seek>(
    reader: &mut R,
) -> Result<DirectoryTree, RapportError> {
    let header = parse_header(reader)?;
    let table = build_fat(reader, &header)?;
    let starting_sector = header
        .dir_info
        .first_sectors
        .first()
        .expect("expected directory sector");
    let sectors = traverse_chain(&table, *starting_sector).chain;
    let len = sectors.len() * FAT_SECTOR_SIZE;
    let mut cursor = Cursor::new(vec![0u8; len]);
    extract_chain(reader, sectors, &mut cursor)?;

    let dir_sectors = extract_dir_entries(&mut cursor)?;
    Ok(dir_sectors.clone().try_into()?)
}
