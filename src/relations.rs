/// A Relation ⊆ T×T
pub trait Relation<T>: Sized {
    fn holds(&self, one: T, other: T) -> bool;

    fn op(&self) -> Self {
        unimplemented!()
        // OpRelationAdapter { relation: &self }
    }
}

// TODO implement checks for right totality

pub trait PartialFunction<T: Eq> {
    fn right_value(&self, one: T) -> Option<T>;
}

// TODO implement left restricted relation
// TODO do we need a domain? → Yes, then we can try to deduce left totality


// Implement `relation` for everything implementing `partialfunction`
impl<Ty: Eq, Tr: PartialFunction<Ty>> Relation<Ty> for Tr {
    fn holds(&self, one: Ty, other: Ty) -> bool {
        self.right_value(one)
    }
    use super::{PartialFunction, Relation};
    use std::collections::HashMap;

    impl PartialFunction<u32> for HashMap<u32, u32> {
        fn right_value(&self, one: u32) -> Option<u32> {
            self.get(&one).map(|&s| s)
        }
    }

    #[test]
    fn test_getting_value() {
        let mut x: HashMap<u32, u32> = HashMap::new();
        x.insert(1, 2);
        x.insert(2, 3);
        x.insert(3, 3);
        assert_eq!(x.right_value(1), Some(2));
        assert_eq!(x.holds(1, 2), true);
        assert_eq!(x.holds(2, 3), true);
        assert_eq!(x.holds(3, 3), true);
    }
}
